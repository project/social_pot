SOCIAL POT MODULE
=================

This module provides an ability to pull posts from social networks.

Features
========

* Pulls posts from Facebook pages.
* Pulls posts from Twitter user timeline (supports retweets and replies).
* Provides a page and a block with social network posts.
* Allows pre-moderation and post-moderation.
* Provides an ability to rearrange social posts in the output.
* Allows to archive old posts automatically.
* Allows to pull posts in cron or using Drush command.

Dependencies
============

* DraggableViews (https://www.drupal.org/project/draggableviews)
* Facebook SDK for PHP (v5) (https://github.com/facebook/php-graph-sdk)
* J7mbo/twitter-api-php (https://github.com/J7mbo/twitter-api-php)

Patches
=======
* DraggableViews: Allow sort handler to select the view that stored the order (https://www.drupal.org/files/issues/2020-04-24/draggableviews-sort_handler_specify_order_view-2767437-70.patch)

Installation
============

```
composer require drupal/social_pot
```

Upgrade
=======

There is no upgrade path from version 1.0.x to 2.0.x. You need to uninstall old version of the module and install the new one.

Admin UI
========

Module configuration page is available at /admin/config/services/social-pot.
Imported social posts can be found at /admin/config/services/social-pot/posts/order.
You can manually add new posts at /node/add/social_post.

UI
==

You can view social posts at /social-posts. Also Social Posts block is available to use.

Drush
=====

To import posts from social networks using Drush, run `drush social-pot:pull` or `drush spp`.

Configuration
=============

Facebook
--------

1. Sign in to https://developers.facebook.com.
2. Go to "My Apps" section.
3. Create new app.
4. Go to your app page.
5. Go to Settings->Basic.
6. Copy App ID and Secret and paste into corresponding fields at /admin/config/services/social-pot.
7. Go to Facebook Graph API Explorer at https://developers.facebook.com/tools/explorer/.
8. Click Generate Access Token button. Click Edit Settings button and select pages you want to grant access to. Click continue.
9. Make sure following permissions are set: pages_show_list, pages_read_engagement.
10. Copy the Access Token and use it to get the Long Term Access Token.
11. Access following URL to get the Long Term Access Token: https://graph.facebook.com/{graph-api-version}/oauth/access_token?grant_type=fb_exchange_token&client_id={app-id}&client_secret={app-secret}&fb_exchange_token={your-access-token}. Graph API version is "v2.10".
12. Paste the Long Term Access Token into Default Access Token field at /admin/config/services/social-pot.
13. For each page you want to import posts from, you need to get Page Access Token because Long Term Access Token is only valid for 60 days.
14. For each page access following URL: https://graph.facebook.com/{page-id}?fields=access_token&access_token={long-term-access-token}.
15. Paste Page ID and it's token into corresponding field at admin/config/services/social-pot.

Twitter
-------

1. Apply for a Twitter API access at https://developer.twitter.com/en/apply-for-access.
2. Create new app at https://developer.twitter.com/en/portal/dashboard.
3. Go to your app page.
4. Go to "Keys and tokens" section.
5. Copy API Key and Secret and paste into corresponding fields at /admin/config/services/social-pot.
6. Generate Access Token and Secret, copy and paste into corresponding fields at /admin/config/services/social-pot.
7. Enter Twitter screen names into corresponding field at /admin/config/services/social-pot.
