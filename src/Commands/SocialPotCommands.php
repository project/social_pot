<?php

namespace Drupal\social_pot\Commands;

use Drupal;
use Drush\Commands\DrushCommands;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use Drupal\social_pot\Importer\TwitterImporter;
use Drupal\social_pot\Importer\FacebookImporter;
use Drupal\social_pot\Archiver\ViewArchiver;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class SocialPotCommands extends DrushCommands {

  /**
   * Pull posts from social networks.
   *
   * @command social-pot:pull
   * @aliases spp
   */
  public function pullPosts() {
    $config = \Drupal::config(SocialPotSettingsForm::SETTINGS);

    $facebookImporter = FacebookImporter::create(\Drupal::getContainer());
    $facebookImporter->import();

    $twitterImporter = TwitterImporter::create(\Drupal::getContainer());
    $twitterImporter->import();

    $twitterImporter = ViewArchiver::create(\Drupal::getContainer());
    $twitterImporter->archive();
  }
}
