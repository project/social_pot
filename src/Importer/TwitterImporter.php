<?php
namespace Drupal\social_pot\Importer;

use Drupal\social_pot\Importer\ImporterBase;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use TwitterAPIExchange;
use Exception;

/**
 * Provides Twitter Importer.
 */
class TwitterImporter extends ImporterBase {
  /**
   * Imports posts.
   */
  public function import() {
    if (!($this->config->get("twitter_pull") ?? SocialPotSettingsForm::TWITTER_PULL)) {
      return;
    }

    \Drupal::messenger()->addMessage("Pulling posts from Twitter.");
    \Drupal::logger('social_pot')->info("Pulling posts from Twitter.");

    $count = $this->importFromUserTimeline();

    parent::import();

    \Drupal::messenger()->addMessage("Pulled ${count['created']} new Twitter posts.");
    \Drupal::logger('social_pot')->info("Pulled ${count['created']} new Twitter posts.");

    \Drupal::messenger()->addMessage("Updated ${count['updated']} existing Twitter posts.");
    \Drupal::logger('social_pot')->info("Updated ${count['updated']} existing Twitter posts.");
  }

  /**
   * Import posts from user timeline.
   */
  public function importFromUserTimeline() {
    $to_update = $this->config->get('general_update_posts') ?? SocialPotSettingsForm::GENERAL_UPDATE_POSTS;
    $screen_names = explode("\n", $this->config->get('twitter_user_timeline_screen_names'));
    $count = $this->config->get('twitter_user_timeline_count') ?? SocialPotSettingsForm::TWITTER_USER_TIMELINE_COUNT;
    $include_retweets = ($this->config->get('twitter_user_timeline_include_retweets') ?? SocialPotSettingsForm::TWITTER_USER_TIMELINE_INCLUDE_RETWEETS) ? 'true' : 'false';
    $exclude_replies = ($this->config->get('twitter_user_timeline_include_replies') ?? SocialPotSettingsForm::TWITTER_USER_TIMELINE_INCLUDE_REPLIES) ? 'false' : 'true';

    $total_count = array(
      'created' => 0,
      'updated' => 0
    );

    $twitter = new TwitterAPIExchange($this->getTwitterSettings());

    foreach ($screen_names as $screen_name) {
      $screen_name = trim($screen_name);

      if (empty($screen_name)) {
        continue;
      }

      $posts_json = $twitter->setGetfield("?screen_name=$screen_name&count=$count&include_rts=$include_retweets&exclude_replies=$exclude_replies&tweet_mode=extended")
        ->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
        ->performRequest();

      $posts = json_decode($posts_json);

      if (!empty($posts->errors)) {
        $message = t('Error requesting data from Twitter: %error.', array(
          '%error' => $posts->errors[0]->message
        ));
        \Drupal::messenger()->addError($message);
        \Drupal::logger('social_pot')->error($message->render());
        continue;
      }

      $posts = array_reverse($posts);

      foreach ($posts as $post) {
        if (!empty($post->retweeted_status)) {
          $id_str = $post->retweeted_status->id_str ?? '';
          $wall_screen_name = $post->user->screen_name ?? '';
          $wall_real_name = $post->user->name ?? '';
          $user_screen_name = $post->retweeted_status->user->screen_name ?? '';
          $user_real_name = $post->retweeted_status->user->name ?? '';
          $media_url = $post->retweeted_status->entities->media[0]->media_url_https ?? '';
          $full_text = $post->retweeted_status->full_text ?? '';
          $created_at = $post->retweeted_status->created_at ?? '';
        } else {
          $id_str = $post->id_str ?? '';
          $wall_screen_name = $user_screen_name = $post->user->screen_name ?? '';
          $wall_real_name = $user_real_name =  $post->user->name ?? '';
          $media_url = $post->entities->media[0]->media_url_https ?? '';
          $full_text = $post->full_text ?? '';
          $created_at = $post->created_at ?? '';
        }

        $social_post = $this->getCreateSocialPost($id_str, 'twitter');

        if (!$to_update && !empty($social_post->get('field_social_post_id')->getValue())) {
          continue;
        }

        $total_count[empty($social_post->get('field_social_post_id')->getValue()) ? 'created' : 'updated']++;

        $social_post
          ->set('title', $this->convertTextToTitle($full_text))
          ->set('field_social_post_type' , ['value' => 'twitter'])
          ->set('field_social_post_id' , ['value' => $id_str])
          ->set('field_social_post_link', ['uri' => 'https://twitter.com/' . $user_screen_name . '/status/' . $id_str])
          ->set('field_social_post_wall_link', ['uri' => 'https://twitter.com/' . $wall_screen_name, 'title' => $wall_real_name])
          ->set('field_social_post_creator_link', ['uri' => 'https://twitter.com/' . $user_screen_name, 'title' => $user_real_name])
          ->set('field_social_post_body', $this->convertTextToValueFormat($full_text))
          ->set('created', strtotime($created_at));

          try {
            $this->downloadAttachImage($social_post, $media_url);
          } catch (Exception $e) {
            $message = t('Error attaching image: %error.', array(
              '%error' => $e->getMessage()
            ));
            \Drupal::messenger()->addError($message);
            \Drupal::logger('social_pot')->error($message->render());
          }

          $social_post->save();
      }
    }

    return $total_count;
  }

  public function getTwitterSettings() {
    return array(
      'oauth_access_token' => $this->config->get('twitter_oauth_access_token'),
      'oauth_access_token_secret' => $this->config->get('twitter_oauth_access_token_secret'),
      'consumer_key' => $this->config->get('twitter_consumer_key'),
      'consumer_secret' => $this->config->get('twitter_consumer_secret')
    );
  }
}
