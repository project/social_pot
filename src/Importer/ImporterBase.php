<?php
namespace Drupal\social_pot\Importer;

use Drupal\Core\Controller\ControllerBase;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Component\Utility\Html;
use Drupal\editor\Entity\Editor;
use Drupal\file\Entity\File;
use Drupal\Component\Utility\Unicode;
use GuzzleHttp\Client as GuzzleClient;
use DateTime;
use Exception;
use stdClass;


/**
 * Provides the ImporterBase.
 */
class ImporterBase extends ControllerBase {
  var $config;

  var $configFactory;
  var $entityTypeManager;

  function __construct(ConfigFactory $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;

    $this->config = $this->configFactory->get(SocialPotSettingsForm::SETTINGS);
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  public function import() {

  }

  /**
   * Get or create Social Post.
   */
  public function getCreateSocialPost($social_id, $type): EntityInterface {
    $query = $this->entityTypeManager->getStorage('node');
    $entityIds = $query->getQuery()
      ->condition('type', 'social_post')
      ->condition("field_social_post_id", $social_id)
      ->condition("field_social_post_type", $type)
      ->execute();

    $entityId = reset($entityIds);

    $status = $this->config->get('general_publish_posts') ? 1 : 0;

    return (!empty($entityId) && is_numeric($entityId))
      ? $this->entityTypeManager->getStorage('node')->load($entityId)
      : $this->entityTypeManager->getStorage('node')->create(['type' => 'social_post'])->set('status', $status);
  }

  /**
   * Check if URL exists;
   */
  public function urlExists($url) {
    $client   = new GuzzleClient(['allow_redirects' => ['track_redirects' => true]]);
    try {
      $client->head($url);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      return false;
    }
    return true;
  }


  /**
   * Download and attach an image to an entity.
   */
  public function downloadAttachImage(EntityInterface $entity, $uri) {
    $id_str = empty($entity->id()) ? "n/a" : $entity->id();

    // Get file URI.
    if (empty($uri)) {
      $this->deleteImage($entity);
      return;
    }

    // Get file contents.
    if (!$this->urlExists($uri)) {
      throw new Exception('file does not exist: ' . $uri . ', id: ' . $id_str);
    }
    $data = file_get_contents($uri);
    if (!$data) {
      throw new Exception('could not download file: ' . $uri . ', id: ' . $id_str);
    }

    // Check if existing file is identical to this one.
    if (!empty($entity->get('field_social_post_image')->first())) {
      $existing_fid = $entity->get('field_social_post_image')->first()->getValue()['target_id'];
      if (!empty($existing_fid)) {
        $existing_file = File::load($existing_fid);
        $existing_file_content = file_get_contents($existing_file->getFileUri());
        if (sha1($data) == sha1($existing_file_content)) {
          return;
        }
      }
    }

    // Save a file.
    list($filename,) = explode('?', basename($uri));
    $filename = (new DateTime())->getTimestamp() . '-' . md5($filename) . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    $path = SocialPotSettingsForm::IMAGE_PATH;
    \Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = file_save_data($data, $path . $filename, FileSystemInterface::EXISTS_REPLACE);
    if (!$file) {
      throw new Exception('could not save file: ' . $uri . ', id: ' . $id_str);
    }

    // Attach file to entity.
    $entity->set('field_social_post_image' , ['target_id' => $file->id()]);
  }

  /**
   * Delete an image from an entity.
   */
  public function deleteImage(EntityInterface $entity) {
    if (!empty($entity->get('field_social_post_image')->first())) {
      $existing_fid = $entity->get('field_social_post_image')->first()->getValue()['target_id'];
      if (!empty($existing_fid)) {
        \Drupal::service('file_system')->delete($existing_fid);
        $entity->set('field_social_post_image' , ['target_id' => null]);
      }
    }
  }

  /**
   * Returns text and text format to be saved as a body field.
   */
  public function convertTextToValueFormat($text) {
    $field = array(
      'value' => $text
    );

    if (class_exists('Drupal\editor\Entity\Editor')) {
      $editors = Editor::loadMultiple();
      if (!empty($editors)) {
        $formats = array_keys($editors);

        if (in_array('basic_html', $formats)) {
          $field['format'] = 'basic_html';
        } else {
          $field['format'] = array_shift($formats);
        }

        $filter = new stdClass();
        $filter->settings = array(
          'filter_url_length' => 50
        );

        $field['value'] = _filter_url(Html::escape($field['value']), $filter);
      }
    }

    return $field;
  }

  /**
   * Returns text to be saved as a title.
   */
  public function convertTextToTitle($text) {
    return Unicode::truncate($text, 255, true, true);
  }
}
