<?php
namespace Drupal\social_pot\Importer;

use Drupal\social_pot\Importer\ImporterBase;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use Facebook\Facebook;
use Exception;

/**
 * Provides Twitter Importer.
 */
class FacebookImporter extends ImporterBase {
  /**
   * Imports posts.
   */
  public function import() {
    if (!($this->config->get("facebook_pull") ?? SocialPotSettingsForm::FACEBOOK_PULL)) {
      return;
    }

    \Drupal::messenger()->addMessage("Pulling posts from Facebook.");
    \Drupal::logger('social_pot')->info("Pulling posts from Facebook.");

    $count = $this->importFromPageFeed();

    parent::import();

    \Drupal::messenger()->addMessage("Pulled ${count['created']} new Facebook posts.");
    \Drupal::logger('social_pot')->info("Pulled ${count['created']} new Facebook posts.");

    \Drupal::messenger()->addMessage("Updated ${count['updated']} existing Facebook posts.");
    \Drupal::logger('social_pot')->info("Updated ${count['updated']} existing Facebook posts.");
  }

  /**
   * Import posts from timeline.
   */
  public function importFromPageFeed() {
    $to_update = $this->config->get('general_update_posts') ?? SocialPotSettingsForm::GENERAL_UPDATE_POSTS;
    $page_ids_tokens = explode("\n", $this->config->get('facebook_page_feed_page_ids_tokens'));
    $count = $this->config->get('facebook_page_feed_count') ?? SocialPotSettingsForm::FACEBOOK_PAGE_FEED_COUNT;

    $total_count = array(
      'created' => 0,
      'updated' => 0
    );

    try {
      $fb = new Facebook($this->getFacebookSettings());
    } catch (Exception $e) {
      $message = t('Error connecting to Facebook: %error.', array(
        '%error' => $e->getMessage()
      ));
      \Drupal::messenger()->addError($message);
      \Drupal::logger('social_pot')->error($message->render());
      return $total_count;
    }

    foreach ($page_ids_tokens as $page_id_token) {
      list($page_id, $page_token) = explode('|', $page_id_token);
      $page_id = trim($page_id);
      $page_token = trim($page_token);

      if (empty($page_id)) {
        continue;
      }

      try {
        $response = $fb->get(
          "/$page_id/posts?fields=id,parent_id,message,created_time,full_picture,from&limit=$count",
          $page_token ?? null
        );
      } catch (Exception $e) {
        $message = t('Error requesting data from Facebook: %error.', array(
          '%error' => $e->getMessage()
        ));
        \Drupal::messenger()->addError($message);
        \Drupal::logger('social_pot')->error($message->render());
        continue;
      }

      try {
        $posts = $response->getDecodedBody()['data'];
      } catch (Exception $e) {
        $message = t('Error processing data from Facebook: %error.', array(
          '%error' => $e->getMessage()
        ));
        \Drupal::messenger()->addError($message);
        \Drupal::logger('social_pot')->error($message->render());
        continue;
      }

      foreach ($posts as $post) {
        $social_post = $this->getCreateSocialPost($post['id'], 'facebook');

        if (!$to_update && !empty($social_post->get('field_social_post_id')->getValue())) {
          continue;
        }

        $total_count[empty($social_post->get('field_social_post_id')->getValue()) ? 'created' : 'updated']++;

        $social_post
          ->set('title', $this->convertTextToTitle($post['message']))
          ->set('field_social_post_type' , ['value' => 'facebook'])
          ->set('field_social_post_id' , ['value' => $post['id']])
          ->set('field_social_post_link', ['uri' => 'https://www.facebook.com/' . $post['id']])
          ->set('field_social_post_wall_link', ['uri' => 'https://www.facebook.com/' . $post['from']['id'], 'title' => $post['from']['name']])
          ->set('field_social_post_creator_link', ['uri' => 'https://www.facebook.com/' . $post['from']['id'], 'title' => $post['from']['name']])
          ->set('field_social_post_body', $this->convertTextToValueFormat($post['message']))
          ->set('created', strtotime($post['created_time']));

        try {
          $this->downloadAttachImage($social_post, $post['full_picture'] ?? '');
        } catch (Exception $e) {
          $message = t('Error attaching image: %error.', array(
            '%error' => $e->getMessage()
          ));
          \Drupal::messenger()->addError($message);
          \Drupal::logger('social_pot')->error($message->render());
        }

        $social_post->save();
      }
    }

    return $total_count;
  }

  public function getFacebookSettings() {
    return array(
      'app_id' => $this->config->get('facebook_app_id'),
      'app_secret' => $this->config->get('facebook_app_secret'),
      'default_access_token' => $this->config->get('facebook_access_token'),
      'default_graph_version' => 'v2.3'
    );
  }
}
