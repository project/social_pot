<?php

namespace Drupal\social_pot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Configure Search API Elasticsearch Synonym.
 */
class SocialPotSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'social_pot.settings';

  const GENERAL_CRON_PULL = FALSE;
  const GENERAL_PUBLISH_POSTS = TRUE;
  const GENERAL_UPDATE_POSTS = FALSE;
  const GENERAL_KEEP_COUNT = 0;

  const FACEBOOK_PULL = FALSE;
  const FACEBOOK_PAGE_FEED_COUNT = 10;

  const TWITTER_PULL = FALSE;
  const TWITTER_USER_TIMELINE_COUNT = 10;
  const TWITTER_USER_TIMELINE_INCLUDE_RETWEETS = TRUE;
  const TWITTER_USER_TIMELINE_INCLUDE_REPLIES = FALSE;

  const IMAGE_PATH = 'public://social-pot/';


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_pot_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['info'] = array(
      '#type' => 'item',
      '#markup' => $this->t('This form allows to configure Social Pot module. You can also @view and @reorder imported social posts.', array(
        '@view' => Link::fromTextAndUrl($this->t('view'), Url::fromUri('internal:/social-posts', array()))->toString(),
        '@reorder' => Link::fromTextAndUrl($this->t('reorder'), Url::fromUri('internal:/admin/config/services/social-pot/posts/order', array()))->toString()
      )),
    );

    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('General'),
    );

    $form['general']['general_cron_pull'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Pull posts in cron'),
      '#default_value' => $config->get('general_cron_pull') ?? static::GENERAL_CRON_PULL
    );

    $form['general']['general_publish_posts'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically publish new posts'),
      '#default_value' => $config->get('general_publish_posts') ?? static::GENERAL_PUBLISH_POSTS
    );

    $form['general']['general_update_posts'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Update existing posts'),
      '#default_value' => $config->get('general_update_posts') ?? static::GENERAL_UPDATE_POSTS,
      '#description' => $this->t('This option won\'t affect the published status.'),
    );

    $form['general']['general_keep_count'] = [
      '#type' => 'select',
      '#title' => $this->t('Keep specific number of posts'),
      '#options' => [
        '0' => $this->t('All'),
        '10' => $this->t('10'),
        '25' => $this->t('25'),
        '50' => $this->t('50'),
        '100' => $this->t('100'),
        '250' => $this->t('250'),
        '500' => $this->t('500'),
        '1000' => $this->t('1000'),
      ],
      '#default_value' => $config->get('general_keep_count') ?? static::GENERAL_KEEP_COUNT,
      '#description' => $this->t('Setting this option will keep top and most recent posts. All other posts will be unpublished and archived.'),
    ];

    $form['facebook'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Facebook'),
    );

    $form['facebook']['facebook_pull'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Pull posts from Facebook'),
      '#default_value' => $config->get('facebook_pull') ?? static::FACEBOOK_PULL
    );

    $form['facebook']['page_feed'] = array(
      '#type' => 'details',
      '#title' => $this->t('Pull from page feed'),
      '#open' => TRUE,
    );

    $form['facebook']['page_feed']['facebook_page_feed_page_ids_tokens'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Facebook page ids and access tokens'),
      '#description' => $this->t('Enter pairs of Facebook page id and page access token (optional) separated by newline. Each line should be in the format of "id|token" or "id".'),
      '#default_value' => $config->get('facebook_page_feed_page_ids_tokens')
    );

    $form['facebook']['page_feed']['facebook_page_feed_count'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Post count'),
      '#description' => $this->t('An amount of posts to pull per time. Maximum is 100.'),
      '#default_value' => $config->get('facebook_page_feed_count') ?? static::FACEBOOK_PAGE_FEED_COUNT
    );

    $form['facebook']['credentials'] = array(
      '#type' => 'details',
      '#title' => $this->t('Facebook credentials'),
      '#open' => FALSE,
    );

    $form['facebook']['credentials']['facebook_app_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $config->get('facebook_app_id')
    );

    $form['facebook']['credentials']['facebook_app_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('App Secret'),
      '#default_value' => $config->get('facebook_app_secret'),
    );

    $form['facebook']['credentials']['facebook_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Default Access Token'),
      '#description' => $this->t('Default Access Token is used when Page Access Token is not provided. It is recommended to use Page Access Token as Default Access Token is valid for 60 days and Page Access Token is valid forever.'),
      '#default_value' => $config->get('facebook_access_token'),
      '#maxlength' => 256
    );

    $form['twitter']['credentials']['twitter_oauth_access_token_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Token Secret'),
      '#description' => $this->t('Also known as Access Token Secret.'),
      '#default_value' => $config->get('twitter_oauth_access_token_secret')
    );

    $form['twitter'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Twitter'),
    );

    $form['twitter']['twitter_pull'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Pull posts from Twitter'),
      '#default_value' => $config->get('twitter_pull') ?? static::TWITTER_PULL
    );

    $form['twitter']['user_timeline'] = array(
      '#type' => 'details',
      '#title' => $this->t('Pull from user timeline'),
      '#open' => TRUE,
    );

    $form['twitter']['user_timeline']['twitter_user_timeline_screen_names'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Twitter screen names'),
      '#description' => $this->t('Enter Twitter screen names separated by newline. Do not include "@" character.'),
      '#default_value' => $config->get('twitter_user_timeline_screen_names')
    );

    $form['twitter']['user_timeline']['twitter_user_timeline_count'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Tweet count'),
      '#description' => $this->t('An amount of tweets to pull per time. Maximum is 200. It is better to set more than you need as Twitter API does not always respect this value.'),
      '#default_value' => $config->get('twitter_user_timeline_count') ?? static::TWITTER_USER_TIMELINE_COUNT
    );

    $form['twitter']['user_timeline']['twitter_user_timeline_include_retweets'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Include retweets'),
      '#default_value' => $config->get('twitter_user_timeline_include_retweets') ?? static::TWITTER_USER_TIMELINE_INCLUDE_RETWEETS
    );

    $form['twitter']['user_timeline']['twitter_user_timeline_include_replies'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Include replies'),
      '#default_value' => $config->get('twitter_user_timeline_include_replies') ?? static::TWITTER_USER_TIMELINE_INCLUDE_REPLIES
    );

    $form['twitter']['credentials'] = array(
      '#type' => 'details',
      '#title' => $this->t('Twitter credentials'),
      '#open' => FALSE,
    );

    $form['twitter']['credentials']['twitter_oauth_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Token'),
      '#description' => $this->t('Also known as Access Token.'),
      '#default_value' => $config->get('twitter_oauth_access_token')
    );

    $form['twitter']['credentials']['twitter_oauth_access_token_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('OAuth Token Secret'),
      '#description' => $this->t('Also known as Access Token Secret.'),
      '#default_value' => $config->get('twitter_oauth_access_token_secret')
    );

    $form['twitter']['credentials']['twitter_consumer_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Key'),
      '#description' => $this->t('Also known as API Key.'),
      '#default_value' => $config->get('twitter_consumer_key')
    );

    $form['twitter']['credentials']['twitter_consumer_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Consumer Secret'),
      '#description' => $this->t('Also known as API Secret Key.'),
      '#default_value' => $config->get('twitter_consumer_secret')
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('general_cron_pull', $form_state->getValue('general_cron_pull'))
      ->set('general_publish_posts', $form_state->getValue('general_publish_posts'))
      ->set('general_update_posts', $form_state->getValue('general_update_posts'))
      ->set('general_keep_count', $form_state->getValue('general_keep_count'))

      ->set('facebook_pull', $form_state->getValue('facebook_pull'))
      ->set('facebook_page_feed_page_ids_tokens', $form_state->getValue('facebook_page_feed_page_ids_tokens'))
      ->set('facebook_page_feed_count', $form_state->getValue('facebook_page_feed_count'))
      ->set('facebook_app_id', $form_state->getValue('facebook_app_id'))
      ->set('facebook_app_secret', $form_state->getValue('facebook_app_secret'))
      ->set('facebook_access_token', $form_state->getValue('facebook_access_token'))

      ->set('twitter_pull', $form_state->getValue('twitter_pull'))
      ->set('twitter_user_timeline_screen_names', $form_state->getValue('twitter_user_timeline_screen_names'))
      ->set('twitter_user_timeline_count', $form_state->getValue('twitter_user_timeline_count'))
      ->set('twitter_user_timeline_include_retweets', $form_state->getValue('twitter_user_timeline_include_retweets'))
      ->set('twitter_user_timeline_include_replies', $form_state->getValue('twitter_user_timeline_include_replies'))
      ->set('twitter_oauth_access_token', $form_state->getValue('twitter_oauth_access_token'))
      ->set('twitter_oauth_access_token_secret', $form_state->getValue('twitter_oauth_access_token_secret'))
      ->set('twitter_consumer_key', $form_state->getValue('twitter_consumer_key'))
      ->set('twitter_consumer_secret', $form_state->getValue('twitter_consumer_secret'))

      ->save();

    parent::submitForm($form, $form_state);
  }

}
