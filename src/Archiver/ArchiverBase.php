<?php
namespace Drupal\social_pot\Archiver;

use Drupal\Core\Controller\ControllerBase;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactory;


/**
 * Provides the ArchiverBase.
 */
class ArchiverBase extends ControllerBase {
  var $config;

  var $configFactory;
  var $entityTypeManager;

  function __construct(ConfigFactory $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;

    $this->config = $this->configFactory->get(SocialPotSettingsForm::SETTINGS);
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  public function archive() {

  }
}
