<?php
namespace Drupal\social_pot\Archiver;

use Drupal\social_pot\Archiver\ArchiverBase;
use Drupal\social_pot\Form\SocialPotSettingsForm;
use Drupal\views\Views;
use Exception;

/**
 * Archive old posts.
 */
class ViewArchiver extends ArchiverBase {
  /**
   * Archive old posts.
   */
  public function archive() {
    $keep_count = $this->config->get("general_keep_count") ?? SocialPotSettingsForm::GENERAL_KEEP_COUNT;

    if ($keep_count == 0) {
      return;
    }

    \Drupal::messenger()->addMessage("Archiving old posts.");
    \Drupal::logger('social_pot')->info("Archiving old posts.");

    $view = Views::getView('social_pot_admin');
    $view->setDisplay('page_order');
    $view->execute();

    $result_to_archive = array_slice($view->result, $keep_count);
    $ids_to_archive = array();

    if (!empty($result_to_archive)) {
      foreach ($result_to_archive as $row) {
        $ids_to_archive[] = $row->nid;
      }

      $posts_to_archive = $this->entityTypeManager->getStorage('node')->loadMultiple($ids_to_archive);

      foreach ($posts_to_archive as $post) {
        $post->set('status', 0)->set('field_social_post_archived', 1)->save();
      }
    }

    $count = count($ids_to_archive);

    \Drupal::messenger()->addMessage("Archived ${count} posts.");
    \Drupal::logger('social_pot')->info("Archived ${count} posts.");

  }
}
